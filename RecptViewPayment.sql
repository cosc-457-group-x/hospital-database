Select p.Payment_Num,p.payment_amount, p.payment_date, pat.Patient_SSN, pat.FNAME, pat.LNAME, pa.Insurance_Name
From Payment p
Inner join collects c on c.Payment_Num=p.Payment_Num
Inner join receptionist r on r.Receptionist_SSN=c.Receptionist_SSN
Inner join payed pa on pa.Payment_Num=p.Payment_Num
Inner join bill b on b.Bill_Num=pa.Bill_Num
Inner join charged ch on b.Bill_Num = ch.Bill_Num
Inner join patients pat on pat.PATIENT_SSN= ch.Patient_SSN
Where r.Receptionist_SSN='000000001'