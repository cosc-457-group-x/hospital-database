Select blocks.Block_Num, blocks.Location, d.Department_Num, d.Department_Name
From blocks
Inner join located_in li on li.BLOCK_NUM=blocks.Block_Num
Inner join department d on d.Department_Num=li.Department_Num
Inner join employee e on e.Department_Num=d.Department_Num
Where e.ssn = '000000003';