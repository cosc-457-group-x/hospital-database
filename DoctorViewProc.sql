Select p.Procedure_Num, p.procedure_type, p.Procedure_Location
From Doctors d
Inner join oversees o on o.doctor_ssn =d.doctor_ssn
Inner join procedures p on p.procedure_num = o.procedure_num
Inner join occurs_in oi on oi.Procedure_Num = p.Procedure_Num
Inner join operating_room oproom on oproom.oproom_num = oi.oproom_num