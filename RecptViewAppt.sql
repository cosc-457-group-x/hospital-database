Select a.Appointment_Num,p.PATIENT_SSN, p.FNAME,p.LNAME, a.Appointment_Date, a.Reason_for_visit, d.Doctor_SSN, e.FNAME, e.Lname
From appointment a
Inner join scheduling s on a.Appointment_Num=s.Appointment_Num
Inner join receptionist r on r.Receptionist_SSN=s.Receptionist_SSN
Inner join scheduled_for sf on sf.Appointment_Num = a.Appointment_Num
Inner join patients p on p.Patient_SSN = sf.Patient_SSN
Inner join scheduled_to st on a.Appointment_Num=st.Appointment_Num
Inner join doctors d on st.Doctor_SSN=d.Doctor_SSN
Inner join employee e on d.Doctor_SSN=e.SSN
