<?php require "header.php";
require "nav/driverleftnav.php";
include("includes/dbh.inc.php");

$id = $_SESSION['id'];  // Grabs Nurses SSN
$sql = "Select blocks.Block_Num, blocks.Location, d.Department_Num, d.Department_Name
From blocks
Inner join located_in li on li.BLOCK_NUM=blocks.Block_Num
Inner join department d on d.Department_Num=li.Department_Num
Inner join employee e on e.Department_Num=d.Department_Num
Where e.ssn = '$id'";
$result = mysqli_query($conn, $sql);
$row = mysqli_fetch_assoc($result);
if (mysqli_num_rows($result) > 0){
    echo '
<div class="main">
    <center>
    <h1>Your Primary Department</h1>
    <table border="1">
    <tr>
    <th>Department Number</th>
    <th>Department Name</th>
    <th>Block Number</th>
    <th>Block Location</th>
    </tr>
    <tr>
    <td>'.$row["Department_Num"].'</td>
    <td>'.$row["Department_Name"].'</td>
    <td>'.$row["Block_Num"].'</td>
    <td>'.$row["Location"].'</td>
    </tr>
    </table>
    </center>
</div>';
}