<?php require "header.php";
require "nav/docleftnav.php";
include("includes/dbh.inc.php");

$patient = mysqli_real_escape_string($conn,$_POST['ssn']);
// Need to make it so that we can $POST a patient SSN from a previous page that makes this info specific to that patient. Perhaps the doctor selects it somehow?
$sql = "Select h.diagnosis_num, d.Diagnosis_Name, d.Diagnosis_Type, d.Diagnosis_Date
From MEDICAL_HISTORY  h
Inner join DIAGNOSIS d on d.Diagnosis_Num = h.Diagnosis_Num
Where h.Patient_SSN = '$patient'";

$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0){
    echo '
<div class="main">
    <center>
    <h1>Patient Medical History</h1>
    <table border="1">
    <tr>
    <th>Diagnosis Number</th>
    <th>Diagnosis Name</th>
    <th>Diagnosis Type</th>
    <th>Diagnosis Date</th>
    </tr>';
    while($row = mysqli_fetch_assoc($result)){
        echo '
    <tr>
    <td>'.$row["diagnosis_num"].'</td>
    <td>'.$row["Diagnosis_Name"].'</td>
    <td>'.$row["Diagnosis_Type"].'</td>
    <td>'.$row["Diagnosis_Date"].'</td>
    </tr>';
    }
    echo '
    </table>
    </center>
</div>';
}?>