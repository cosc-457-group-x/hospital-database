<?php require "header.php";
require "nav/docleftnav.php";
include("includes/dbh.inc.php");

$id = $_SESSION['id'];  // Grabs Doctors SSN

$sql = "Select d.diagnosis_num, d.diagnosis_name, d.diagnosis_type,d.diagnosis_date
From Diagnosis d
Inner join diagnosed_by db on db.Diagnosis_Num=d.Diagnosis_Num
Inner join doctors dt on dt.Doctor_SSN= db.Doctor_SSN
Where dt.Doctor_SSN = '$id'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0){
    echo '
<div class="main">
    <center>
    <h1>Diagnoses assigned by yourself</h1>
    <table border="1">
    <tr>
    <th>Diagnosis Number</th>
    <th>Diagnosis Name</th>
    <th>Diagnosis Type</th>
    <th>Diagnosis Date</th>
    </tr>';
    while($row = mysqli_fetch_assoc($result)){
   echo '
    <tr>
    <td>'.$row["diagnosis_num"].'</td>
    <td>'.$row["diagnosis_name"].'</td>
    <td>'.$row["diagnosis_type"].'</td>
    <td>'.$row["diagnosis_date"].'</td>
    </tr>'; }
    echo '
    </table>
    </center>
</div>';
}