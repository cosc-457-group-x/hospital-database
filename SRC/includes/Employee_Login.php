<?php
   include("dbh.inc.php");
   session_start();
   $error = null;
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $mySSN = mysqli_real_escape_string($conn,$_POST['SSN']);
      $sql = "SELECT SSN FROM EMPLOYEE WHERE SSN = '$mySSN'";
      $result = mysqli_query($conn,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      
      $count = mysqli_num_rows($result);
      
      // If the result matched on the entered SSN, 1 row will be present
      if($count == 1) {
         session_start();
         $_SESSION['id'] = $mySSN;
         $doc = "SELECT DOCTOR_SSN FROM DOCTORS INNER JOIN EMPLOYEE ON DOCTORS.DOCTOR_SSN = EMPLOYEE.SSN WHERE EMPLOYEE.SSN = '$mySSN'";
         $recep = "SELECT RECEPTIONIST_SSN FROM RECEPTIONIST INNER JOIN EMPLOYEE ON RECEPTIONIST.RECEPTIONIST_SSN = EMPLOYEE.SSN WHERE EMPLOYEE.SSN = '$mySSN'";
         $driver = "SELECT DRIVER_SSN FROM DRIVER INNER JOIN EMPLOYEE ON DRIVER.DRIVER_SSN = EMPLOYEE.SSN WHERE EMPLOYEE.SSN = '$mySSN'";
         $nurse = "SELECT NURSE_SSN FROM NURSE INNER JOIN EMPLOYEE ON NURSE.NURSE_SSN = EMPLOYEE.SSN WHERE EMPLOYEE.SSN = '$mySSN'";
         
        $result = mysqli_query($conn,$doc); // If the query returns a row, that means the employee is a doctor and they will be redirected to the doctors page
        if (mysqli_num_rows($result) > 0)
        {
            header("Location: ../doctors.php");
        }
        $result = mysqli_query($conn,$recep); // If the query returns a row, that means the employee is a receptionist and they will be redirected to the receptionist page
        if (mysqli_num_rows($result) > 0)
        {
            header("Location: ../receptionist.php"); 
        }
        $result = mysqli_query($conn,$driver); // If the query returns a row, that means the employee is a driver and they will be redirected to the drivers page
        if (mysqli_num_rows($result) > 0)
        {
            header("Location: ../driver.php");
        }
        $result = mysqli_query($conn,$nurse); // If the query returns a row, that means the employee is a nurse and they will be redirected to the nurses page
        if (mysqli_num_rows($result) > 0)
        {
            header("Location: ../nurse.php");
        }
      }else {           // No rows were returned, which means the SSN entered does not exist within the system
         $error = "Your SSN is invalid";
      }
   }
?>
<html>
   
   <head>
      <title>Employee Login Page</title>
      
      <style type = "text/css">
         body {
            font-family:Arial, Helvetica, sans-serif;
            font-size:14px;
         }
         label {
            font-weight:bold;
            width:100px;
            font-size:14px;
         }
         .box {
            border:#666666 solid 1px;
         }
      </style>
      
   </head>
   
   <body bgcolor = "#FFFFFF">
	
      <div align = "center">
         <div style = "width:300px; border: solid 1px #333333; " align = "left">
            <div style = "background-color:#333333; color:#FFFFFF; padding:3px;"><b>Employee Login</b></div>
				
            <div style = "margin:30px">
               
               <form action = "" method = "post">
                  <label>SSN : </label><input type = "text" name = "SSN" class = "box"/><br /><br />
                  <input type = "submit" value = " Submit "/><br />
               </form>
               
               <?php if($error != null) { ?>
               <div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php echo $error; ?></div>
				<?php }?>
            </div>
				
         </div>
			
      </div>

   </body>
</html>