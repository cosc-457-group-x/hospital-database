<?php
   include("dbh.inc.php");
   session_start();
   $error = null;
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $mySSN = mysqli_real_escape_string($conn,$_POST['SSN']);
      $sql = "SELECT PATIENT_SSN FROM PATIENTS WHERE PATIENT_SSN = '$mySSN'";
      $result = mysqli_query($conn,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      
      $count = mysqli_num_rows($result);
      
      // If result matched $myusername and $mypassword, table row must be 1 row
		
      if($count == 1) {
         session_start();
         $_SESSION['id'] = $mySSN;
         
         header("location: ../patient.php?");
      }else {
         $error = "Your SSN is invalid";
      }
   }
?>
<html>
   
   <head>
      <title>Patient Login Page</title>
      
      <style type = "text/css">
         body {
            font-family:Arial, Helvetica, sans-serif;
            font-size:14px;
         }
         label {
            font-weight:bold;
            width:100px;
            font-size:14px;
         }
         .box {
            border:#666666 solid 1px;
         }
      </style>
      
   </head>
   
   <body bgcolor = "#FFFFFF">
	
      <div align = "center">
         <div style = "width:300px; border: solid 1px #333333; " align = "left">
            <div style = "background-color:#333333; color:#FFFFFF; padding:3px;"><b>Patient Login</b></div>
				
            <div style = "margin:30px">
               
               <form action = "" method = "post">
                  <label>SSN : </label><input type = "text" name = "SSN" class = "box"/><br /><br />
                  <input type = "submit" value = " Submit "/><br />
               </form>
               
                <div style = "font-size:16px; color:#000000; margin-top:10px">New Patient? <a href="../signup.php?">Sign-up here!</a></div>
                
               <?php if($error != null) { ?>
               <div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php echo $error; ?></div>
				<?php }?>
            </div>
				
         </div>
			
      </div>

   </body>
</html>