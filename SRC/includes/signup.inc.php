<?php

    require 'dbh.inc.php';

    // Escape user inputs for security
    $SSN = mysqli_real_escape_string($conn, $_REQUEST['SSN']);
    $Fname = mysqli_real_escape_string($conn, $_REQUEST['First_Name']);
    $Lname = mysqli_real_escape_string($conn, $_REQUEST['Last_Name']);
    $Sex = mysqli_real_escape_string($conn, $_REQUEST['Sex']);
    $DOB = mysqli_real_escape_string($conn, $_REQUEST['DOB']);
    $Address = mysqli_real_escape_string($conn, $_REQUEST['Address']);
    
    
    if (empty($SSN) || empty($Fname) || empty($Lname) || empty($Sex) || empty($DOB) || empty($Address)) {
        header("Location: ../signup.php?error=emptyfields&SSN=".$SSN);
        exit();
    } 
    // Attempt insert query execution
    $sql = "INSERT INTO PATIENTS (PATIENT_SSN, FNAME, LNAME, SEX, DATE_OF_BIRTH, ADDRESS) VALUES ('$SSN', '$Fname', '$Lname', '$Sex', '$DOB', '$Address')";
    if(mysqli_query($conn, $sql)){
        header("Location: signupdiagnosis.php?SSN=".$SSN);
    } else{
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
    }
    
    // Close connection
    mysqli_close($conn);

    ?>
