<?php require "header.php";
require "nav/patientleftnav.php";
include("includes/dbh.inc.php");

$id = $_SESSION['id'];  // Grabs Doctors SSN

$sql = "Select a.Appointment_Num, a.Appointment_Date, a.Reason_for_visit, e.FNAME, e.Lname
From appointment a
Inner join scheduling s on a.Appointment_Num=s.Appointment_Num
Inner join receptionist r on r.Receptionist_SSN=s.Receptionist_SSN
Inner join scheduled_for sf on sf.Appointment_Num = a.Appointment_Num
Inner join patients p on p.Patient_SSN = sf.Patient_SSN
Inner join scheduled_to st on a.Appointment_Num=st.Appointment_Num
Inner join doctors d on st.Doctor_SSN=d.Doctor_SSN
Inner join employee e on d.Doctor_SSN=e.SSN
WHERE p.Patient_SSN = '$id'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0){
    echo '
    <div class="main">
    <center>
    <h1>Appointments you have scheduled</h1>
    <table border="1">
       <tr>
    <th>Appointment Number</th>
    <th>Appointment Date</th>
    <th>Reason for Visit</th>
    <th>Doctor</th>
    </tr>
    <tr>';
    while($row = mysqli_fetch_assoc($result)){
   echo '
    <td>'.$row["Appointment_Num"].'</td>
    <td>'.$row["Appointment_Date"].'</td>
    <td>'.$row["Reason_for_visit"].'</td>
    <td>Dr. '.$row["FNAME"].' '.$row["Lname"].'</td>
    </tr>'; }
   echo '
    </table>
    </center>
</div>';
}