<?php require "header.php";
require "nav/receptionistnav.php";
include("includes/dbh.inc.php");

$id = $_SESSION['id'];  // Grabs Doctors SSN
$sql = "Select a.Appointment_Num,p.PATIENT_SSN, p.FNAME,p.LNAME, a.Appointment_Date, a.Reason_for_visit, e.fname, e.lname
From appointment a
Inner join scheduled_for sf on sf.Appointment_Num = a.Appointment_Num
Inner join patients p on p.Patient_SSN = sf.Patient_SSN
Inner join scheduled_to st on a.Appointment_Num=st.Appointment_Num
Inner join doctors d on st.Doctor_SSN=d.Doctor_SSN
Inner join employee e on d.Doctor_SSN=e.SSN";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0){
    echo '
<div class="main">
    <center>
    <h1>All Scheduled Appointments</h1>
    <table border="1">
    <tr>
    <th>Appointment Number</th>
    <th>Patient SSN</th>
    <th>Patient First Name</th>
    <th>Patient Last Name</th>
    <th>Appointment Date</th>
    <th>Reason for Visit</th>
    <th>Doctor</th>
    </tr>
    <tr>';
    while($row = mysqli_fetch_assoc($result)){
   echo '
    <td>'.$row["Appointment_Num"].'</td>
    <td>'.$row["PATIENT_SSN"].'</td>
    <td>'.$row["FNAME"].'</td>
    <td>'.$row["LNAME"].'</td>
    <td>'.$row["Appointment_Date"].'</td>
    <td>'.$row["Reason_for_visit"].'</td>
    <td>Dr. '.$row["fname"].' '.$row["lname"].'</td>
    </tr>'; }
   echo '
    </table>
    </center>
</div>';
}