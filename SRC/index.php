<?php require "header.php"; ?>

<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata">
<style>
body, html {
  height: 100%;
  font-family: "Inconsolata", sans-serif;
}

.bgimg {
  background-position: center;
  background-size: cover;
  background-image: url("img/intermountain-medical-center.jpg");
  min-height: 75%;
}

.menu {
  display: none;
}
</style>
<body>

<!-- Header with image -->
<header class="bgimg w3-display-container w3-grayscale-min" id="home">
  <div class="w3-display-middle w3-center">
    <span class="w3-text-white" style="font-size:90px">Next<br>Best<br>Hospital</span>
  </div>
</header>

<!-- Add a background color and large text to the whole page -->
<div class="w3-sand w3-grayscale w3-large">

<!-- About Container -->
<div class="w3-container" id="about">
  <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-64"><span class="w3-tag w3-wide">OUR PURPOSE</span></h5>
    <p> Providing exemplary physical, emotional and spiritual care for each of our patients and their families. </p>
    <p>Balancing the continued commitment to the care of the poor and those most in need with the provision of highly specialized services to a broader community. </p>
    <p>Building a work environment where each person is valued, respected and has an opportunity for personal and professional growth. </p>
    <p>Advancing excellence in health services education. </p>
    <p>Fostering a culture of discovery in all of our activities and supporting exemplary health sciences research.</p>
    <img src="/img/patientanddoctor.jpg" style="width:100%;max-width:1000px" class="w3-margin-top">
    <p><strong>Hours:</strong> 24 Hrs, 7 days a week</p>
    <p><strong>Address:</strong> 101 Hospital Dr, 2019, MD</p>
  </div>
</div>


<script>
// Tabbed Menu
function openMenu(evt, menuName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("menu");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(menuName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-dark-grey";
}
document.getElementById("myLink").click();
</script>

</body>
</html>
