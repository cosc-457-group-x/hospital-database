<?php require "header.php";
require "nav/receptionistnav.php";
include("includes/dbh.inc.php");

$patient = mysqli_real_escape_string($conn,$_POST['ssn']);
$sql = "Select p.Patient_SSN, p.FNAME, p.LNAME, h.Appointment_Num
From GENERAL_HISTORY h
Inner join SCHEDULING s on h.Appointment_Num = s.Appointment_Num
Inner join RECEPTIONIST r on s.Receptionist_SSN = r.Receptionist_SSN
Inner join Patients p on h.Patient_SSN = p.Patient_SSN
Where h.Patient_SSN = '$patient'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0){
    echo '
<div class="main">
    <center>
    <h1>Patient General History</h1>
    <table border="1">
    <tr>
    <th>Patient SSN</th>
    <th>Patient First Name</th>
    <th>Patient Last Name</th>
    <th>Appointment Number</th>
    </tr>';
    while($row = mysqli_fetch_assoc($result)){
   echo '
    <tr>   
    <td>'.$row["Patient_SSN"].'</td>
    <td>'.$row["FNAME"].'</td>
    <td>'.$row["LNAME"].'</td>
    <td>'.$row["Appointment_Num"].'</td>
    </tr>'; }
   echo '
    </table>
    </center>
</div>';
}