<?php require "header.php";
require "nav/receptionistnav.php";
include("includes/dbh.inc.php");

$id = $_SESSION['id'];  // Grabs Nurses SSN
$sql = "Select p.Payment_Num,p.payment_amount, p.payment_date, pat.Patient_SSN, pat.FNAME, pat.LNAME
From Payment p
left join payed pa on pa.Payment_Num=p.Payment_Num
left join bill b on b.Bill_Num=pa.Bill_Num
Inner join charged ch on b.Bill_Num = ch.Bill_Num
Inner join patients pat on pat.PATIENT_SSN= ch.Patient_SSN";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0){
    echo '
<div class="main">
    <center>
    <h1>Processed Payments:</h1>
    <table border="1">
    <tr>
    <th>Payment Number</th>
    <th>Payment Amount</th>
    <th>Payment Date</th>
    <th>Patient SSN</th>
    <th>Patient First Name</th>
    <th>Patient Last Name</th>
    </tr>
    <tr>';
    while($row = mysqli_fetch_assoc($result)){
   echo '
    <td>'.$row["Payment_Num"].'</td>
    <td>'.$row["payment_amount"].'</td>
    <td>'.$row["payment_date"].'</td>
    <td>'.$row["Patient_SSN"].'</td>
    <td>'.$row["FNAME"].'</td>
    <td>'.$row["LNAME"].'</td>
    </tr>
'; }
    echo '   
    </table>
    </center>
</div>';
}