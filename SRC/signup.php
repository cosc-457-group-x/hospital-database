<?php $error = null;

if (isset($_GET['error'])) {
    if ($_GET['error'] == "emptyfields") {
        $error = "Please fill in all required fields";
    }
}
?>
<html>

<head>
<title>Patient Signup Page</title>

<style type = "text/css">
body {
    font-family:Arial, Helvetica, sans-serif;
    font-size:14px;
}
label {
    font-weight:bold;
    width:100px;
    font-size:14px;
}
.box {
    border:#666666 solid 1px;
}
</style>

</head>

<body bgcolor = "#FFFFFF">

<div align = "center">
<div style = "width:300px; border: solid 1px #333333; " align = "left">
<div style = "background-color:#333333; color:#FFFFFF; padding:3px;"><b>Patient Signup</b></div>

<div style = "margin:30px">

<form action="includes/signup.inc.php" method="post">
    <input type="text" name="SSN" placeholder="SSN">
    <input type="text" name="First_Name" placeholder="First Name">
    <input type="text" name="Last_Name" placeholder="Last Name">
    <input type="text" name="Sex" placeholder="Sex"> 
    <input type="text" name="DOB" placeholder="Date Of Birth">
    <input type="text" name="Address" placeholder="Address"><br></br>
    <button type="submit" name="signup-submit">Signup</button>
</form>
</form>

<?php if($error != null) { ?>
               <div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php echo $error; ?></div>
				<?php }?>
            </div>	
         </div>
      </div>
   </body>
</html>

