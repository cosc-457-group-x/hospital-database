Select p.fname, p.lname,d.diagnosis_num, d.diagnosis_name, d.diagnosis_type,d.diagnosis_date
From Medical_history mh
Inner join Patients p on p.patient_ssn = mh.patient_ssn
Inner join diagnosis d on d.diagnosis_num = mh.diagnosis_num
