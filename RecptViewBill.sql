Select b.Bill_Num,b.amount,b.due_date, p.PATIENT_SSN,p.Fname,p.Lname
From Bill b
Inner join issues i on b.Bill_Num=i.Bill_Num
Inner join receptionist r on r.Receptionist_SSN=i.Receptionist_SSN
Inner join charged c on c.Bill_Num=b.Bill_Num
Inner join patients p on p.Patient_SSN=c.Patient_SSN
Where r.Receptionist_SSN='000000001'