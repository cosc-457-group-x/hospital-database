Select h.Patient_SSN, p.FNAME, p.Lname, h.Appointment_Num, a.Appointment_Date, a.Reason_for_visit
From GENERAL_HISTORY h
Inner join SCHEDULING s on h.Appointment_Num = s.Appointment_Num
Inner join appointment a on a.Appointment_Num= s.Appointment_Num
Inner join scheduled_for sf on sf.Appointment_Num = a.Appointment_Num
Inner join patients p on p.Patient_SSN = sf.Patient_SSN
Inner join RECEPTIONIST r on s.Receptionist_SSN = r.Receptionist_SSN
Where  h.Patient_SSN = '000000010';